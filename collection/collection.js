BrodcastMessages = new Meteor.Collection("broadcastMessages");
SavedMessages=new Meteor.Collection("savedMessages");

var Schemas = {};




Schemas.comment=new SimpleSchema({

	message:{
		type:String
	},

	user:{
		type:String
	},

	createdAt: {
    	type: Date,
      	

      }



});

Schemas.message= new SimpleSchema({

	msg:{
		type:Schemas.comment
	},
	comments:{
		type:[Schemas.comment],
		optional:true
	},
	approvers:{
		type:[String],
		optional:true

	},

	disapprovers:{
		type:[String],
		optional:true
	}

});

Schemas.savedMessage= new SimpleSchema({
	msgId:{
		type:String
	},
	savedBy:{
		type:String
	}

});


BrodcastMessages.attachSchema(Schemas.message);
SavedMessages.attachSchema(Schemas.savedMessage);
