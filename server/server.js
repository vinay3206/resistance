Meteor.methods({
        

        createBrodcastMessage:function(serviceObj){
        	var dbObject={
        		msg:serviceObj,
        		comments:[],
        		approvers:[],
        		disapprovers:[],
        	};

        	BrodcastMessages.insert(dbObject);

        },

        createsaveMessage:function(messageId){
        	var dbObject={
        		msgId:messageId,
        		savedBy:Meteor.userId()
        	};
        	SavedMessages.insert(dbObject);


        },

        addComment:function(messageId,comment){

            BrodcastMessages.update(messageId, {$push: {comments: comment}});
        },

        approve:function(messageId){
            var message=BrodcastMessages.findOne(messageId);
            var approvers=message.approvers;
            var disapprovers=message.disapprovers;

            // remove user from disApprovers if their
            var disapproverIndex=disapprovers.indexOf(Meteor.userId());
            if(disapproverIndex!=-1){
                disapprovers.splice(disapproverIndex,1);
            }

            // check if user is already in approved then no need to push him again
            if(approvers.indexOf(Meteor.userId())==-1){
                approvers.push(Meteor.userId());

            }
            BrodcastMessages.update(messageId,{$set:{approvers:approvers,disapprovers:disapprovers}});

        },

        disApprove:function(messageId){

            var message=BrodcastMessages.findOne(messageId);
            var approvers=message.approvers;
            var disapprovers=message.disapprovers;

            // remove user from Approvers if their
            var approversIndex=approvers.indexOf(Meteor.userId());
            if(approversIndex!=-1){
                approvers.splice(approversIndex,1);
            }

            // check if user is already in disapproved then no need to push him again
            if(disapprovers.indexOf(Meteor.userId())==-1){
                disapprovers.push(Meteor.userId());

            }
            BrodcastMessages.update(messageId,{$set:{approvers:approvers,disapprovers:disapprovers}});

        }

        

    });