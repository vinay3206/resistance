if (Meteor.isClient) {
  Router.map(function () {
    this.route('home', {
      path: '/',
    });
    this.route('saved',{path:'saved'});
    this.route('login',{path:'login'});
    this.route('signup',{path:'signup'});
    

  });


  UI.registerHelper('displayName', function(userId) {
    
    var user= Meteor.users.findOne(userId);
    if(user)
      return user.profile.name;
    return '';


  });

  UI.registerHelper('rank', function(userId) {
    
    var user= Meteor.users.findOne(userId);
    if(user)
      return user.profile.rank;
    return '';


  });

  UI.registerHelper('dateFormat', function(date) {
    
    var date= new Date(date);
    return date.toDateString() +' '+date.toLocaleTimeString();


  });

  UI.registerHelper('isDisabled', function() {
    
    if(Meteor.user()){
      return '';
    }
    return 'disabled';


  });


  



}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
