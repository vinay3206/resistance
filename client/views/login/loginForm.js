 var validateEmail=function(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

 var validateSignIn=function(t){
    var isValid=true;
    var error=[];
    var email = t.find('#signin-email').value
        , password = t.find('#signin-password').value;
       
        isValid=validateEmail(email);
        if(!isValid){
          error.push("Email is inValid");
        }
        
        if(isValid){
          Session.set('signinError',[]);
        }
        else{
          Session.set('signinError',error);
        }



    return isValid;

  };



Template.loginForm.events({

 

	'submit .form-signin' : function(e, t) {
      e.preventDefault();
      if(validateSignIn(t)){
        var email = t.find('#signin-email').value
          , password = t.find('#signin-password').value;
         

         // If validation passes, supply the appropriate fields to the
          // Meteor.loginWithPassword() function.
          Meteor.loginWithPassword(email, password, function(err){
          
            if(err){
              var signinError=[];
              signinError.push(err.reason);
              Session.set('signinError',signinError);
              
                console.log(err);
            }else{
              Router.go('home');
            }
            
        });
      }

      return false;
    }


});


Template.loginForm.signinError=function(){
  return Session.get('signinError');
};

