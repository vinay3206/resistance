Template.message.events({


	'click .btn-save-message':function(e,t){
		
		Meteor.call('createsaveMessage',this.message._id);

	},

	'click .btn-approve':function(e,t){
		Meteor.call('approve',this.message._id);
	},

	'click .btn-disapprove':function(e,t){
		Meteor.call('disApprove',this.message._id);
	},

	'click .btn-add-comment':function(e,t){

		$(t.find('.div-add-comment')).removeClass('hidden');


	},

	'click .btn-cancel-comment':function(e,t){
		$(t.find('.div-add-comment')).addClass('hidden');

	},

	'click .btn-save-comment':function(e,t){
		var message=t.find('.txt-comment').value.trim();
		if(message){
			var commentObj={
				message:message,
				user:Meteor.userId(),
				createdAt:new Date()

			};
			Meteor.call('addComment',this.message._id,commentObj);
			$(t.find('.div-add-comment')).addClass('hidden');
			t.find('.txt-comment').value='';
		}

	},

	'click .btn-hide-comments':function(e,t){
		$(t.find('.div-comments')).addClass('hidden');
		$(t.find('.btn-hide-comments')).addClass('hidden');
		$(t.find('.btn-show-comments')).removeClass('hidden');

	},

	'click .btn-show-comments':function(e,t){
		$(t.find('.div-comments')).removeClass('hidden');
		$(t.find('.btn-hide-comments')).removeClass('hidden');
		$(t.find('.btn-show-comments')).addClass('hidden');

	}


	
	
});

Template.message.isLeader=function(){

	var user=Meteor.users.findOne(this.message.msg.user);
	if(user && (user.profile.rank=="Resistance Leader")){
		return true;
	}
	return false;

};
