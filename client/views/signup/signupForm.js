 var validateEmail=function(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };  

 var validateSignUp=function(t){
    var isValid=true;
    var error=[];
    var email = t.find('#signup-email').value
        , password = t.find('#signup-password').value
        ,rank=t.find('#signup-rank').value
        ,confirmPassword=t.find('#signup-confirmPassword').value
        ,displayName=t.find('#signup-displayName').value
        isValid=validateEmail(email);
        if(!isValid){
          error.push("Email is inValid");
        }
        if(confirmPassword!=password){
          isValid=false;
          error.push("password and confirm password Dont match");
        }
        if(isValid){
          Session.set('signupError',[]);
        }
        else{
          Session.set('signupError',error);
        }



    return isValid;

  };

  


Template.signupForm.events({

 


	'submit .form-signup' : function(e, t) {

      e.preventDefault();
      if(validateSignUp(t)){
        var email = t.find('#signup-email').value
          , password = t.find('#signup-password').value
          ,rank=t.find('#signup-rank').value
          ,displayName=t.find('#signup-displayName').value;

         
         Accounts.createUser({email: email, password : password,profile:{name:displayName,rank:rank}}, function(err){
            if (err) {
              // Inform the user that account creation failed
              
               var signupError=[];
              signupError.push(err.reason);
              Session.set('signupError',signupError);
              
              
              console.log(err);

            } else {
              // Success. Account has been created and the user
              // has logged in successfully.
              // take user to home page
              Router.go('home'); 
            }

          });
     }

      return false;
    }


});

Template.signupForm.signupError=function(){

  return Session.get('signupError');

};